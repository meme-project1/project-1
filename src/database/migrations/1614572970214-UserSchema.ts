import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class UserSchema1614572970214 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: "user",
        columns: [
          {
            name: "id",
            type: "bigint",
            isPrimary: true,
            isGenerated: true,
            generationStrategy: "increment",
          },
          {
            name: "username",
            type: "varchar",
            length: "80",
            isNullable: false,
            isUnique: true,
          },
          {
            name: "email",
            type: "varchar",
            length: "191",
            isNullable: false,
            isUnique: true,
          },
          {
            name: "password",
            type: "varchar",
            length: "255",
            isNullable: false,
          },
          {
            name: "avatar_url",
            type: "varchar",
            length: "255",
            isNullable: true,
          },
          {
            name: "full_name",
            type: "varchar",
            length: "100",
            isNullable: true,
          },
          {
            name: "created_at",
            type: "bigint",
            isNullable: true,
          },
          {
            name: "updated_at",
            type: "bigint",
            isNullable: true,
          },
          {
            name: "is_active",
            type: "tinyint",
            width: 1,
            isNullable: false,
            default: 0,
          },
          {
            name: "is_active_2fa",
            type: "tinyint",
            width: 1,
            isNullable: false,
            default: 0,
          },
          {
            name: "two_factor_authentication_secret",
            type: "varchar",
            length: "255",
            isNullable: true,
          },
        ],
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable("user");
  }
}
