<h1>bitkub-learn-resume
  <a
    href="http://nestjs.com/"
    target="blank"
  >
    <img
      src="https://nestjs.com/img/logo_text.svg"
      width="65"
      alt="Nest Logo"
    />
  </a>
</h1>

## Description

bitkub-learn-resume.

## Installation

1. You must install Node version 16 or newer. To run stable application, we recommend using node version 16->18.

2. Install all required packages

```bash
 $ npm install
```

## Setup

1. Create `.env` file using `env.example`

Note: Please set `prod-api` to run `api server` and `prod-worker` to run `worker server` in NODE_ENV for production environment.

```bash
# APP
PORT=4000 # API port
SOCKET_PORT=3003


NODE_ENV= prod-api #prod-worker

#JWT
JWT_SECRET_KEY=bitkub-learn-resume-jwt-key # your secret key, anything is ok
JWT_EXPIRATION_TIME=1d
JWT_REFRESH_TOKEN_EXPIRATION_TIME=2d

# TYPEORM
TYPEORM_CONNECTION=mysql
TYPEORM_HOST=localhost # MySQL host
TYPEORM_PORT=3306 # MySQL port
TYPEORM_USERNAME=root # MySQL username
TYPEORM_PASSWORD=1 # MySQL password
TYPEORM_DATABASE=nft_land # schema name
TYPEORM_MIGRATIONS_DIR=src/database/migrations
TYPEORM_MIGRATIONS=dist/database/migrations/*.js
TYPEORM_ENTITIES_DIR=dist/**/*.entity.js
IS_ASYNC=true # for the first run application should be set true
IS_LOGGING=true

# MAP CONFIG
SIZE_MAP=400
TOTAL_R1=99000
TOTAL_R2=13000
TOTAL_R3=1000

#BLOCK REQUEST
LIMIT_REQUEST=5
LIMIT_HOURS_BLOCK_REQUEST=4



#GOOGLE CREDENTIAL
GOOGLE_OAUTH_CLIENT_ID=143434324218-chu273aaq93fdjfhdfdsfkhd6.apps.googleusercontent.com
GOOGLE_OAUTH_SECRET=Ghdfo32r32r32pbQAwkQ
GOOGLE_OAUTH_CALLBACK_URL=http://localhost:3000/api/auth/callback/google

#REDIS
REDIS_HOST=localhost
REDIS_PORT=10001
REDIS_DB_CACHE=8

# INIT SUPER ADMIN ACCOUNT
SUPER_ADMIN_EMAIL=super-admin@var-meta.com
SUPER_ADMIN_FULL_NAME='super-admin'

# PRIVATE KEY ADMIN WALLET ADDRESS
PRIVATE_KEY=71769296477db5f62a5dc997b2334a11f6f769bd6247b6db98368a700c95c48a
BLOCK_CONFIRM=1
```

2. Create database structure

Run the app for the first time to create the database structure.

3. Setup required data on database

These datas are for the testnet and  should be changed to be suitable for the mainnet.

```bash
INSERT INTO `currency_config` (`id`, `network`, `chain_name`, `chain_id`, `average_block_time`, `required_confirmations`, `temp_required_confirmations`, `rpc_endpoint`, `explorer_endpoint`, `created_at`, `updated_at`, `scan_api`) VALUES
(1, 'BSC', 'BSC Testnet', '97', 3000, 15, 0, 'wss://bsc-testnet.nodereal.io/ws/v1/c655688583b24a4bb43afeae54de8e52', 'https://testnet.bscscan.com/', NULL, NULL, 'https://api-testnet.bscscan.com');

INSERT INTO `game_info` (`id`, `name`, `chain_id`, `address`, `payment_token`, `admin`, `start_time`, `end_time`, `conquer_time`, `created_at`, `updated_at`) VALUES
(1, 'ConquerGame', '97', '0x04849ef949440550122e5876e1a5a9f673f2f8c5', '0x84ced597c29517841C91F589b9AE45d3157CfbC4', '0x0954Dfb5b4d37c91671903249dC6E2959315722f', 1701363600, 1703264399, 15, NULL, 1701939662570);
```

4. Import country and pixel data into your database

We will give you a country and pixel CSV file. You just need to download the below-attached files, then import them into your database.

- For country data link: <a href="https://drive.google.com/file/d/1lVwTI-lilmN9b-i7F-w4KG3EA10yfKQA/view?usp=sharing">playbux-countries-data</a>

- For pixels data link: <a href="https://drive.google.com/file/d/14mEVtXKf4zIc2gD8sUZtJ-KKXC4UD1x1/view?usp=drive_link">playbux-pixels-data</a>

## Running the app

```bash
# build
$ npm run build

# deploy
$ npm run start:prod
```

## License

This project is under [MIT licensed](LICENSE).

## Gen SSL

You need to generate sslcert with your domain and replace sslcert folder.
