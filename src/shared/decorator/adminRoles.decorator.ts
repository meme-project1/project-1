import { AdminRoles } from "../enums";
import { SetMetadata } from "@nestjs/common";

export const ROLES_KEY = "roles";
export const AdminRolesAllowed = (...roles: AdminRoles[]) =>
  SetMetadata(ROLES_KEY, roles);
