import { config } from "dotenv";

config();
import { getConnectionManager } from "typeorm";
import { NestFactory } from "@nestjs/core";
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";
import { AppModule } from "./app.module";
import { debugLog, logger } from "./shared/logger";
import * as fs from "fs";
import { ValidationPipe } from "node_modules/@nestjs/common";
import {
  ExpressAdapter,
  NestExpressApplication,
} from "@nestjs/platform-express";
import * as bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import express from "express";
import helmet from "helmet";
import cors from "cors";
import { join } from "path";

let nest: NestExpressApplication;
const isNodeProd = process.env.NODE_ENV === "prod-api";
const port = process.env.PORT || 3001;

const gracefulShutdown = () => {
  global["isShutdown"] = true;
  console.info(
    "Got SIGTERM. Graceful shutdown start",
    new Date().toISOString()
  );
  if (nest) {
    nest
      .close()
      .then(() => {
        console.log("Closed out nest app.");
        const dbCon = getConnectionManager().get();
        if (dbCon.isConnected) {
          return dbCon.close();
        }
      })
      .then(() => {
        console.log("Closed out database connection.");
        process.exit(0);
      })
      .catch(() => {
        console.log("Closed out remaining connections.");
        process.exit(0);
      });
  }
  setTimeout(() => {
    console.error(
      "Could not close connections in time, forcefully shutting down"
    );
    process.exit();
  }, 10 * 1000);
};

process.on("uncaughtException", (err) => {
  console.error(err);
});
process.on("unhandledRejection", (err) => {
  console.error(err);
});
process.on("SIGTERM", gracefulShutdown);
process.on("SIGINT", gracefulShutdown);

async function bootstrap() {
  const server = express();
  nest = await NestFactory.create(AppModule, new ExpressAdapter(server));
  if (isNodeProd) {
    nest.set("trust proxy", 1);
  }
  nest.use(
    helmet({
      contentSecurityPolicy: true,
      hidePoweredBy: true,
      referrerPolicy: true,
    })
  );
  nest.use(
    cors({
      origin: true,
      credentials: true,
    })
  );
  nest.useStaticAssets(join(__dirname, "..", "public"), {
    prefix: "/public/",
  });
  nest.use(bodyParser.json({ limit: "5mb" }));
  nest.use(bodyParser.urlencoded({ limit: "5mb", extended: true }));

  if (!isNodeProd) {
    const options = new DocumentBuilder()
      .setTitle("learn-to-earn-resume-api APIs")
      .setDescription("BitKub Learn and Earn Resume APIs")
      .setVersion("1.0")
      .addBearerAuth()
      .build();

    const document = SwaggerModule.createDocument(nest, options);
    writeSwaggerJson(`${process.cwd()}`, document);
    SwaggerModule.setup("api/docs", nest, document);
  }

  nest.use(logger);
  nest.use(cookieParser());
  // nest.enableCors();
  nest.useGlobalPipes(new ValidationPipe({ transform: true }));
  nest.listen(port, () => {
    debugLog(`Application type ${process.env.NODE_ENV} is running on: ${port}`);
  });
}

bootstrap();

export const writeSwaggerJson = (path: string, document) => {
  fs.writeFileSync(`${path}/swagger.json`, JSON.stringify(document, null, 2), {
    encoding: "utf8",
  });
};
