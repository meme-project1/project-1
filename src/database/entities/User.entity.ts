import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
} from "typeorm";

@Entity("users")
export class User {
  @PrimaryGeneratedColumn("uuid", { name: "user_id" })
  id: string;

  @Column({ name: "username", type: "varchar", nullable: true })
  username?: string;

  @Column({ name: "first_name", type: "varchar", nullable: true })
  firstName?: string;

  @Column({ name: "last_name", type: "varchar", nullable: true })
  lastName?: string;

  @Column({ name: "email", type: "varchar", nullable: true })
  email?: string;

  @Column({ name: "social_id", type: "varchar", nullable: true })
  socialId?: string;

  @Column({ name: "phone_number", type: "bigint", nullable: true })
  phoneNumber?: number;

  @Column({ name: "wallet_address", type: "varchar", nullable: true })
  walletAddress?: string;

  @Column({ name: "avatar_url", type: "text", nullable: true })
  avatarUrl?: string;

  @Column({ name: "last_login", type: "timestamp", nullable: true })
  lastLogin?: Date;

  @Column({ name: "is_active", type: "boolean", default: false })
  isActive: boolean;

  @CreateDateColumn({ name: "created_at", type: "timestamp" })
  createdAt: Date;

  @UpdateDateColumn({ name: "updated_at", type: "timestamp" })
  updatedAt: Date;
}
