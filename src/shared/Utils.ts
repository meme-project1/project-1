import { IPaginationOptions, Pagination } from "nestjs-typeorm-paginate";
import * as CryptoJS from "crypto-js";
import NodeCache from "node-cache";
const nodeCache = new NodeCache({ stdTTL: 2, checkperiod: 2 });

export function nowInMillis(): number {
  return Date.now();
}

// Alias for nowInMillis
export function now(): number {
  return nowInMillis();
}

export function nowInSeconds(): number {
  return (nowInMillis() / 1000) | 0;
}

export function addHttps(url: string) {
  if (!/^(?:f|ht)tps?\/\//.test(url)) {
    url = "https://" + url;
  }
  return url;
}

export function checkIPaginationOptions(options: IPaginationOptions): boolean {
  if (options.limit == 0 || options.page == 0) {
    return false;
  }
  return true;
}

export function encrypt(data: string) {
  return CryptoJS.MD5(data).toString();
}

export function convertToString(value: any) {
  return typeof value === "string" ? value : "";
}

export function getArrayPagination<T>(
  totalItems: any[],
  options: any
): Pagination<T> {
  const { limit, page } = options;

  const selectedItems = totalItems.slice((page - 1) * limit, page * limit);
  const pagination = {
    totalItems: totalItems.length,
    itemCount: selectedItems.length,
    itemsPerPage: limit,
    totalPages: Math.ceil(totalItems.length / limit),
    currentPage: page,
  };

  return new Pagination(selectedItems, pagination, null);
}

export function existValueInEnum(type: any, value: any): boolean {
  return (
    Object.keys(type)
      .filter((k) => isNaN(Number(k)))
      .filter((k) => type[k] === value).length > 0
  );
}

export function convertSizeToRank(size: number): string {
  switch (size) {
    case 1:
      return "";
    case 2:
      return "S";
    case 3:
      return "SS";
    default:
      throw new Error("Size is not valid");
  }
}

async function web3Cache(key, func) {
  let value = nodeCache.get(key);
  if (value == undefined) {
    // handle miss!
    value = await func;
    nodeCache.set(key, value);
    return value;
  }
  return value;
}

export async function getBlockNumber(chainId, web3) {
  return web3Cache(`${chainId}: getBlockNumber`, web3.eth.getBlockNumber());
}

export function getLimitItemsFromArray(arr: any[], first: number): any[] {
  if (arr.length <= first) {
    return arr;
  } else {
    return arr.slice(0, first);
  }
}
export function calculateTotal(array, fieldName): number {
  return array.reduce((accumulator, currentValue) => {
    return accumulator + currentValue[fieldName];
  }, 0);
}
export function generateRandomString(length = 25) {
  let result = "";
  const characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

export function arrayUniqueByKey<T>(key: string,data: T[]): T[] {
  const arrayUniqueByKey = [
    ...new Map(data.map((item) => [item[key], item])).values(),
  ];
  return arrayUniqueByKey;
}
