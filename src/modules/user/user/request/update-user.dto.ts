import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsOptional, IsString } from "class-validator";

export class UpdateUserDto {
  @ApiProperty({ example: "namphan.tech" })
  @IsString()
  @IsOptional()
  username?: string;

  @ApiProperty({ example: "var-meta" })
  @IsString()
  @IsOptional()
  firstName?: string;

  @ApiProperty({ example: "admin" })
  @IsString()
  @IsOptional()
  lastName?: string;

  @ApiProperty({ example: "namphan@devtify.tech" })
  @IsEmail()
  @IsOptional()
  email?: string;

  @ApiProperty({ example: "halohalohehe" })
  @IsString()
  @IsOptional()
  password?: string;

  @ApiProperty({ example: "0x72738F57604A5FD4ed86dF1b944A5B5450D69E4b" })
  @IsString()
  @IsOptional()
  walletAddress?: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  avatarUrl?: string;

  @ApiProperty({ example: true })
  isActive?: boolean;
}
