/* eslint-disable @typescript-eslint/no-unused-vars */
export function calculusOrder(x: number, y: number) {
  const width = 400;
  const height = 300;
  return Number(width) * Number(y) + Number(x);
}

export function numberToBinaryString(number) {
  const binaryString = number?.toString(2);
  const padding = "0".repeat(8 - binaryString?.length);
  return padding + binaryString;
}
export function numberToBinaryBuffer(number: number): Buffer {
  const buffer = Buffer.alloc(1);
  buffer.writeUInt8(number, 0);
  return buffer;
}

export function bufferToNumber(binaryByte) {
  const buffer = Buffer.from([binaryByte]);
  return buffer.readUInt8();
}

export function bufferToBinaryString(buffer: Buffer) {
  let binaryString = "";
  for (let i = 0; i < buffer.length; i++) {
    binaryString += buffer[i].toString(2).padStart(8, "0");
  }
  return binaryString;
}

export function replaceStringAtIndex(
  originalString: string,
  startIndex: number,
  endIndex: number,
  replacement: string
) {
  if (
    startIndex < 0 ||
    endIndex >= originalString.length ||
    startIndex > endIndex
  ) {
    return originalString;
  }

  const prefix = originalString.slice(0, startIndex);
  const suffix = originalString.slice(endIndex + 1);
  const replacedString = prefix + replacement + suffix;

  return replacedString;
}

export async function replaceBufferAtIndex(
  originalBuffer: Buffer,
  index: number,
  replacement: Buffer
) {
  if (index < 0 || index >= originalBuffer.length) {
    return originalBuffer;
  }
  const originalArray = Array.from(originalBuffer);
  const replacementArray = Array.from(replacement);
  originalArray[index] = replacementArray[0];
  const replacedBuffer = Buffer.from(originalArray);
  return replacedBuffer;
}
