import { BaseResponse } from "./baseResponse.dto";
import { EmptyObject } from "./emptyObject.dto";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { ApiResponseProperty } from "@nestjs/swagger";

export class EmptyObjectBase extends BaseResponse {
  @ApiResponseProperty({
    type: EmptyObject,
  })
  data: EmptyObject;
}
