# Step 1: Use a Node.js image as the base
FROM node:18.19.0-alpine@sha256:b1a0356f7d6b86c958a06949d3db3f7fb27f95f627aa6157cb98bc65c801efa2 as builder

# Step 2: Update and upgrade apk packages, then install Python and other build dependencies
RUN apk update && apk upgrade && apk add --no-cache python3 py3-pip make g++

# Step 3: Create a symbolic link for Python (if necessary)
RUN if [ ! -e /usr/bin/python ]; then ln -sf python3 /usr/bin/python; fi

# Step 4: Set the working directory
WORKDIR /usr/src/app

# Step 5: Copy package.json, yarn.lock
COPY package.json yarn.lock ./

# Step 6: Install all dependencies (including dev dependencies)
RUN yarn install --frozen-lockfile

# Step 7: Copy the rest of your application's source code
COPY . .

# Step 8: Build your application
RUN yarn build

# Step 9: Install only production dependencies
RUN yarn install --production --frozen-lockfile --ignore-scripts

# Step 10: Use a new, smaller image for the production environment
FROM node:18.19.0-alpine@sha256:b1a0356f7d6b86c958a06949d3db3f7fb27f95f627aa6157cb98bc65c801efa2

# Step 11: Update and upgrade apk packages
RUN apk update && apk upgrade

# Step 12: Set the working directory in the new image
WORKDIR /usr/src/app

# Step 13: Copy the build from the builder stage
COPY --from=builder /usr/src/app/dist ./dist
COPY --from=builder /usr/src/app/node_modules ./node_modules

# Step 14: Set environment variables
ENV NODE_ENV=production

# Step 15: Expose the port your app runs on
EXPOSE 3000

# Step 16: Define the command to run your app
CMD ["node", "dist/main"]
