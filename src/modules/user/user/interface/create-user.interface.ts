export interface createUserInterface {
  username?: string;
  email: string;
  phoneNumber?: number;
  password: string;
  avatarUrl?: string;
}
