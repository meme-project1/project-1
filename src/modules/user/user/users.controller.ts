import { Body, Controller, Delete, Get, Param, Patch } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { User } from "src/database/entities";
import { UpdateUserDto } from "./request/update-user.dto";
import { UsersService } from "./users.service";

@ApiTags("users")
@Controller("api/users")
export class UsersController {
  constructor(private usersService: UsersService) {}
  @Get()
  public async getUsers(): Promise<User[]> {
    return await this.usersService.userRepository.find();
  }

  @Get(":id")
  public async findUser(@Param("id") id: string): Promise<User | undefined> {
    return this.usersService.findUserById(id);
  }

  @Patch(":id")
  public async updateUser(
    @Param("id") id: string,
    @Body() updateUserDto: UpdateUserDto
  ): Promise<User | undefined> {
    const existedUser = await this.usersService.findUserById(id);
    if (existedUser) {
      const fieldsToUpdate: (keyof UpdateUserDto)[] = [
        "username",
        "firstName",
        "lastName",
        "email",
        "walletAddress",
        "avatarUrl",
        "isActive",
        "password",
      ];
      const updateUser: Partial<User> = {};

      fieldsToUpdate.forEach((field: string) => {
        if (updateUserDto[field] !== undefined) {
          updateUser[field] = updateUserDto[field];
        }
      });

      return this.usersService.update(id, updateUser);
    }
  }

  @Delete(":id")
  public remove(@Param("id") id: string): Promise<void> {
    return this.usersService.remove(id);
  }
}
