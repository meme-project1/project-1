import { StoreConfig } from "cache-manager";
import { CacheModuleOptions } from "@nestjs/common/cache/interfaces/cache-module.interface";
import * as redisStore from "cache-manager-redis-store";

export const redisConfig: CacheModuleOptions<StoreConfig> = {
  store: redisStore,
  ttl: 60 * 60 * 1000,
  host: process.env.REDIS_HOST,
  port: process.env.REDIS_PORT,
  db: process.env.REDIS_DB_CACHE,
  auth_pass: process.env.REDIS_PASSWORD,
  isGlobal: true,
};

export const redis_key = {
  MAP_BINARY: "map-binary",
  TOTAL_SPENDING: "total-spending",
  TOTAL_DEFAULT_COUNTRY: "total-default-country",
  COUNTRY_CONQUER_RATIOS: "country-ratios",
  COUNTRY_RANK: "country-rank",
  COUNTRY_CONQUER: "country-conquer",
  COUNTRY_INFO: "country-info",
  CONQUER_HISTORY: "conquer-history",
};

export const redis_ttl = {
  MAP_BINARY_TTL: 10,
  TOTAL_SPENDING_TTL: 10,
  COUNTRY_CONQUER_RATIOS_TTL: 10,
  COUNTRY_RANK_TTL: 10,
  COUNTRY_CONQUER_TTL: 10,
  COUNTRY_INFO_TTL: 10,
  TOTAL_DEFAULT_COUNTRY_TTL: 7 * 24 * 60 * 60,
  CONQUER_HISTORY_TTL: 10,
};
