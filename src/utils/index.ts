export function extractDigitAndUnit(inputString: string) {
  const match = inputString.match(/(\d+)([^\d]*)/); // Match one or more digits and anything that is not a digit

  if (match) {
    const digit: number = parseInt(match[1], 10); // Convert the matched digits to an integer
    const remain: number | string = match[2]; // The remaining string
    return { digit, remain };
  } else {
    return null;
  }
}
