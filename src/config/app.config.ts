import { registerAs } from "@nestjs/config";

export default registerAs("app", () => ({
  ssoServerDomain: process.env.SSO_SERVER_DOMAIN,
  resumeFrontEndDomain: process.env.RESUME_FRONTEND_DOMAIN,
  apiKey: process.env.CLIENT_SECRET_KEY,
  clientId: process.env.CLIENT_ID,
  clientScope: process.env.CLIENT_SCOPE,

  accessTokenKey: `a_${process.env.CLIENT_ID}`,
  refreshTokenKey: `r_${process.env.CLIENT_ID}`,
  smartContractUrl: process.env.SMARTCONTRACT_URL,
}));
