import { ConnectionOptions } from "typeorm";
import {
} from "../database/entities";

export const databaseConfig: ConnectionOptions = {
  type: (process.env.TYPEORM_CONNECTION || "postgres") as any,
  host: process.env.TYPEORM_HOST || "localhost",
  port: parseInt(process.env.TYPEORM_PORT) || 5432,
  username: process.env.TYPEORM_USERNAME,
  password: process.env.TYPEORM_PASSWORD,
  database: process.env.TYPEORM_DATABASE,
  entities: [
  ],
  synchronize: process.env.IS_ASYNC === "true",
  logging: process.env.IS_LOGGING === "true",
};
