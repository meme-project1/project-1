import {
  createParamDecorator,
  ExecutionContext,
} from "node_modules/@nestjs/common";

export const UserDecorator = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    return request.user;
  }
);
