import appConfig from "./config/app.config";
import { databaseConfig } from "./config/database.config";
import { redisConfig } from "./config/redis.config";
import { ConfigModule } from "@nestjs/config";
import { APP_FILTER, APP_INTERCEPTOR } from "@nestjs/core";
import { TypeOrmModule } from "@nestjs/typeorm";
import { CacheModule, Module } from "@nestjs/common";
import { ExceptionFilter } from "./config/exception/exception.filter";
import { TransformInterceptor } from "./config/rest/transform.interceptor";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";

import { UsersModule } from "./modules/user/user/users.module";

@Module({
  imports: [
    CacheModule.register(redisConfig),
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: ".env",
      load: [appConfig],
    }),
    TypeOrmModule.forRoot(databaseConfig),

    UsersModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_INTERCEPTOR,
      useClass: TransformInterceptor,
    },
    {
      provide: APP_FILTER,
      useClass: ExceptionFilter,
    },
  ],
})
export class AppModule {}
