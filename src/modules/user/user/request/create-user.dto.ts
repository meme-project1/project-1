import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsOptional, IsString } from "class-validator";

export class CreateUserDto {
  @ApiProperty({ example: "email@email.com", required: true })
  @IsEmail()
  email: string;

  @ApiProperty({ example: 1234567890, required: false })
  @IsOptional()
  phoneNumber?: number;

  @ApiProperty({ example: "Password123", required: true })
  @IsString()
  password: string;

  @ApiProperty({ example: "Password123", required: true })
  @IsString()
  confirmPassword: string;
}
